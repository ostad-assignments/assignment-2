import { Button, Card, Page, Text } from '@shopify/polaris';
import { useContext } from 'react';
import { AuthContext } from '../utils/context/AuthContext';

const Home = () => {
  const { token } = useContext(AuthContext);

  const getUrl = () => {
    if (!token) {
      return '';
    }
    const payload = token.substring(
      token.indexOf('.') + 1,
      token.lastIndexOf('.')
    );
    const { shop: shopUrl } = JSON.parse(atob(payload));

    return `https://${shopUrl}/admin/themes/current/editor?context=apps`;
  };

  const handleActivate = () => {
    const link = getUrl();
    window.parent.location.href = link;
  };

  return (
    <Page
      title="Home"
      primaryAction={{
        content: 'Activate',
        onAction: handleActivate,
        primary: true,
      }}
    >
      <Card>
        <Text as="p" variant="bodyLg">
          You just need to{' '}
          <Button variant="plain" onClick={handleActivate}>
            activate
          </Button>{' '}
          the app in order to use.
        </Text>
        <Text as="p" variant="bodyLg">
          Once you hit "activate" button, you will be redirected to theme
          customization UI. Please turn on the app and save settings.
        </Text>
        <Text as="p" variant="bodyLg">
          Translation UI will be added on the store header once you activate and
          save :)
        </Text>
      </Card>
    </Page>
  );
};

export default Home;
