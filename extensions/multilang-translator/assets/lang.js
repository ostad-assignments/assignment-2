const config = { childList: true };
const targetId = 'ostad-google_translate_element';

const headerTarget = document.querySelector('header').lastElementChild;
const targetNode = document.createElement('div');

targetNode.setAttribute('id', targetId);
headerTarget.insertBefore(targetNode, headerTarget.firstElementChild);

function googleTranslateElementInit() {
  setTimeout(() => {
    new google.translate.TranslateElement(
      { pageLanguage: Shopify.locale },
      targetId
    );
  }, 2000);
}

const callback = (mutationList, observer) => {
  for (const mutation of mutationList) {
    if (mutation.type === 'childList') {
      const dropdown = mutation.target.querySelector('select');

      if (mutation.target.children[0].isEqualNode(dropdown)) {
        return;
      }

      const appBlock = document.querySelector('.ostad-multilingual');
      appBlock.style.minWidth = '150px';

      mutation.target.removeChild(mutation.target.children[0]);
      mutation.target.appendChild(dropdown);
    }
  }
};

const observer = new MutationObserver(callback);

if (targetNode) {
  observer.observe(targetNode, config);
} else {
  observer.disconnect();
}
